import 'env.dart';

class APIUrl {
  static String api = "${Env.baseUrlSSl}/api";

  static String people = "$api/people";
}