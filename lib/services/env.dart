
class Env {
  static const uri = "swapi.dev";
  static const baseUrlSSl = "https://$uri";
  static const headersReq = {"content-type": "application/json"};
}