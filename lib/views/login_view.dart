import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:provider/provider.dart';
import 'package:star_wars/main.dart';
import 'package:star_wars/providers/user_auth_state.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/views/regis_view.dart';
import 'package:star_wars/widgets/slide_pop_up/slide_pop_up_custom_content.dart';
import 'package:star_wars/widgets/v_button.dart';
import 'package:star_wars/widgets/v_text_input.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState  createState() => _LoginViewState();
}

class _LoginViewState  extends State<LoginView> {
  final _formKey = GlobalKey<FormState>();
  UserAuthState? state;
  String? email;
  String? password;
  @override
  Widget build(BuildContext context) {
    state = Provider.of<UserAuthState>(context);
    return Scaffold(
      backgroundColor: mainBackgroud,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Login"),
      ),
      body: Container(
        color: white,
        padding: paddingInput,
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              VInputText(
                "email",
                  validator: (val) => (val!.length == 0 ? "Please enter a email" :
                      state!.checkEmail(val) ? null : "Please enter a valid email"
                  ),
                  onSaved: (val) => email = val
              ),
              SizedBox(height: 10),
              VInputText(
                  "password",
                  obscureText: true,
                  onSaved: (val) => password = val
              ),
              VButton(
                title: "Login",
                onTap: () {
                  var form = _formKey.currentState;
                  if (form!.validate()) {
                    form.save();
                    state!.login(email!, password!).then((value) {
                      if(value){
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) {
                            return MyApp();
                          },
                        ));
                      } else {
                        SlidePopUpCustomContent(
                          context: context,
                          title: 'Warning',
                          child: Center(
                            child: Text('Email or password invalid', style: titleFont,),
                          ),
                        ).build();
                      }
                    });
                  }
                }
              ),
              TextButton(
                style: TextButton.styleFrom(
                  textStyle: const TextStyle(fontSize: 20),
                ),
                onPressed: () => Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return RegisView();
                  },
                )),
                child: Text('Regis', style: bodyMediumFont),
              ),
            ],
          ),
        ),
      ),
    );
  }
}