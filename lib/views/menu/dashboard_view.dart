import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:provider/provider.dart';
import 'package:star_wars/main.dart';
import 'package:star_wars/providers/dashboard_main_state.dart';
import 'package:star_wars/providers/user_auth_state.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/views/regis_view.dart';
import 'package:star_wars/widgets/decoration.dart';
import 'package:star_wars/widgets/search_bar.dart';
import 'package:star_wars/widgets/slide_pop_up/slide_pop_up_custom_content.dart';
import 'package:star_wars/widgets/slide_pop_up/v_draggable_scrollable_sheet.dart';
import 'package:star_wars/widgets/v_button.dart';
import 'package:star_wars/widgets/v_date_picker.dart';
import 'package:star_wars/widgets/v_text_input.dart';
import 'package:visibility_detector/visibility_detector.dart';

class DashboardView extends StatefulWidget {
  @override
  _DashboardViewState  createState() => _DashboardViewState();
}

class _DashboardViewState  extends State<DashboardView> {
  DashboardMainState? state;
  @override
  Widget build(BuildContext context) {
    return  ChangeNotifierProvider(
      create: (context) => DashboardMainState(),
      child: Consumer(
        builder: (BuildContext context, DashboardMainState state, Widget? child){
          this.state = state;
          return VisibilityDetector(
              key: state.visibilityDetector,
              child: ModalProgressHUD(child: _body(), inAsyncCall: state.isLoading),
              onVisibilityChanged: (visibilityInfo){
                var visibilityPercentage = visibilityInfo.visibleFraction * 100;
                if(visibilityPercentage == 100){state.init();}
              });
        },
      ),
    );
  }

  Widget _body(){
    return Scaffold(
        key: state!.scaffoldState,
        backgroundColor: white,
        body: Container(
          color: white,
          padding: paddingInput,
          child: Column(
            children: [
              SearchBar(
                onChanged: (val) => state!.searchQuaries(val),
              ),
              state!.isGrid ? _grid() : _list(),
            ],
          )
        ),
        floatingActionButton: PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                value: 1,
                onTap: () => state!.changeModeView(),
                child: Row(
                  children: [
                    Icon(state!.isGrid ? Icons.list : Icons.border_all, color: mainBackgroud),
                    Text(state!.isGrid ? 'List View' : 'Grid View', style: bodyMediumFont,)
                  ],
                ),
              ),
              PopupMenuItem(
                value: 2,
                onTap: () => state!.changeOrder(),
                child: Row(
                  children: [
                    Icon(state!.isASC ? Icons.arrow_drop_up_rounded : Icons.arrow_drop_down_rounded, color: mainBackgroud),
                    Text(state!.isASC ? 'DESC' : 'ASC', style: bodyMediumFont,)
                  ],
                ),
              ),
              PopupMenuItem(
                value: 3,
                enabled:false,
                child:  GestureDetector(
                  onTap: () => viewDetails(true),
                  child: Row(
                    children: [
                      Icon(Icons.add, color: mainBackgroud),
                      Text('Add', style: bodyMediumFont,)
                    ],
                  ),
                ),
              ),
            ],
            icon: Container(
              height: 100,
              width: 100,
              decoration: const ShapeDecoration(
                  color: mainBackgroud,
                  shape: StadiumBorder(
                    side: BorderSide(color: Colors.white, width: 2),
                  )
              ),
              child: const Icon(Icons.menu, color: Colors.white),
            )
        )
    );
  }

  Widget _grid(){
    return SingleChildScrollView(
      child: GridView.builder(
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 20/16.7,
              crossAxisSpacing: 2,
              mainAxisSpacing: 2),
          physics: NeverScrollableScrollPhysics(),
          itemCount: state!.displayList.length,
          itemBuilder: (BuildContext ctx, index) {
            return Card(
              color: frame,
              child: ListTile(
                onTap: () {
                  state!.editPeoplesAndDetails(index);
                  viewDetails(false);
                },
                title: Text(state!.displayList[index].name!),
                subtitle: Text(toBeginningOfSentenceCase(state!.displayList[index].gender)!),
                trailing: IconButton(onPressed: () => state!.changeFavorite(index), icon: Icon(Icons.favorite, color: state!.displayList[index].favorite! ? red : gray)),
              ),
            );
          }
      ),
    );
  }

  Widget _list(){
    return SingleChildScrollView(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: ScrollPhysics(),
          itemCount: state!.displayList.length,
          itemBuilder: (BuildContext ctx, index) {
          return Card(
            color: frame,
            child: ListTile(
              onTap: () {
                state!.editPeoplesAndDetails(index);
                viewDetails(false);
              },
              title: Text(state!.displayList[index].name!),
              subtitle: Text(toBeginningOfSentenceCase(state!.displayList[index].gender)!),
              trailing: IconButton(onPressed: () => state!.changeFavorite(index), icon: Icon(Icons.favorite, color: state!.displayList[index].favorite! ? red : gray)),
            ),
          );
        },
      ),
    );
  }

  viewDetails(bool isAdd){
    if(isAdd) state!.addPeoplesAndDetails();
    print(isAdd);
    TextEditingController _birthYear = TextEditingController(text: state!.details.birth_year);
    VDraggableScrollableSheet(
        context: context,
      child: Form(
        key: state!.formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              state!.details.id != null ? IconButton(
                  tooltip: "Delete",
                  onPressed: () {
                    SlidePopUpCustomContent(
                      context: context,
                      title: 'Warning Delete',
                      child: Center(
                          child: Column(
                            children: <Widget>[
                              Text('Are you sure you want to delete ${state!.details.name}?', style: titleFont,),
                              VButton(
                                  title: "Delete",
                                  onTap: () {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    state!.deleteDetails();
                                    state!.init();
                                  }
                              ),
                            ],
                          )
                      ),
                    ).build();
                  },
                  icon: Icon(Icons.restore_from_trash, color: red)) :
              SizedBox(height: 1),
              VInputText(
                  "Name",
                  initialValue: state!.details.name ?? '',
                  onSaved: (val) => state!.details.name = val
              ),
              SizedBox(height: 10),
              VInputText(
                  "Height",
                  isNumber: true,
                  initialValue: state!.details.height ?? '',
                  onSaved: (val) => state!.details.height = val
              ),
              SizedBox(height: 10),
              VInputText(
                  "Mass",
                  isNumber: true,
                  initialValue: state!.details.mass ?? '',
                  onSaved: (val) => state!.details.mass = val
              ),
              SizedBox(height: 10),
              VInputText(
                  "Hair color",
                  initialValue: state!.details.hair_color ?? '',
                  onSaved: (val) => state!.details.hair_color = val
              ),
              SizedBox(height: 10),
              VInputText(
                  "Skin color",
                  initialValue: state!.details.skin_color ?? '',
                  onSaved: (val) => state!.details.skin_color = val
              ),
              SizedBox(height: 10),
              VInputText(
                  "Eye color",
                  initialValue: state!.details.eye_color ?? '',
                  onSaved: (val) => state!.details.eye_color = val
              ),
              SizedBox(height: 10),
              VInputText(
                'at_the_date_of',
                controller: _birthYear,
                readOnly: true,
                onSaved: (val) => state!.details.birth_year = val,
                onTap: () => VDatePicker(
                  context: context,
                  date: DateUtil.stringToDate(date: state!.details.birth_year!),
                  onConfirm: (dateTime, List<int> index) {
                    _birthYear.text = DateUtil.dateToString(date: dateTime);
                  },
                ).build(),
              ),
              SizedBox(height: 10),
              DropdownButtonFormField<String>(
                decoration: inputDec(context,'Gender'),
                validator: (val) =>
                (val == null ? 'Please select' : null),
                value: state!.details.gender,
                icon: Icon(Icons.keyboard_arrow_down),
                iconSize: 20,
                // enabled: _isEnabled,
                onChanged: (String? newValue) {
                  state!.details.gender = newValue;
                },
                items: state!.genders.map<DropdownMenuItem<String>>((value) {
                  return DropdownMenuItem(
                    value: value,
                    child: Text(toBeginningOfSentenceCase(value)!),
                  );
                }).toList(),
              ),
              SizedBox(height: 10),
              VInputText(
                  "Homeworld",
                  initialValue: state!.details.homeworld ?? '',
                  onSaved: (val) => state!.details.homeworld = val
              ),
              VButton(
                  title: "Save",
                  onTap: () {
                    var form = state!.formKey.currentState;
                    if (form!.validate()) {
                      form.save();
                      SlidePopUpCustomContent(
                        context: context,
                        title: 'Warning',
                        child: Center(
                            child: Column(
                              children: <Widget>[
                                Text('Are you sure you want to save?', style: titleFont,),
                                VButton(
                                    title: "Save",
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      state!.saveDetails(isAdd);
                                      state!.init();
                                    }
                                ),
                              ],
                            )
                        ),
                      ).build();
                    }
                  }
              ),
            ],
          ),
        )
      ),
    ).build();
  }
}