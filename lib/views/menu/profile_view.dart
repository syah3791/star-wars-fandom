import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:provider/provider.dart';
import 'package:star_wars/main.dart';
import 'package:star_wars/models/user_model.dart';
import 'package:star_wars/providers/user_auth_state.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/views/regis_view.dart';
import 'package:star_wars/widgets/slide_pop_up/slide_pop_up_custom_content.dart';
import 'package:star_wars/widgets/v_button.dart';
import 'package:star_wars/widgets/v_text_input.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState  createState() => _ProfileViewState();
}

class _ProfileViewState  extends State<ProfileView> {
  UserModel? _user;
  @override
  Widget build(BuildContext context) {
    _user = Provider.of<UserAuthState>(context).user;
    return Scaffold(
        backgroundColor: mainBackgroud,
        body: Wrap(
          children: [
            Card(
              elevation: 3.8,
              margin: paddingList,
              child: Container(
                padding: paddingInput,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    VInputText(
                      "Name",
                      enabled: false,
                      initialValue: _user!.name ?? '',
                    ),
                    SizedBox(height: 10),
                    VInputText(
                      "email",
                      enabled: false,
                      initialValue: _user!.email ?? '',
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
    );
  }
}