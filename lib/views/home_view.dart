import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:star_wars/main.dart';
import 'package:star_wars/providers/user_auth_state.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/views/menu/dashboard_view.dart';
import 'package:star_wars/views/menu/favorite_view.dart';
import 'package:star_wars/views/menu/profile_view.dart';
import 'package:star_wars/widgets/slide_pop_up/slide_pop_up_custom_content.dart';
import 'package:star_wars/widgets/v_button.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState  createState() => _HomeViewState();
}

class _HomeViewState  extends State<HomeView> {
  UserAuthState? state;
  @override
  Widget build(BuildContext context) {
    state = Provider.of<UserAuthState>(context);
    return DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Scaffold(
            backgroundColor: mainBackgroud,
            appBar: AppBar(
                bottom: const TabBar(
                  indicator:UnderlineTabIndicator(
                    borderSide: BorderSide(width: 4.0, color: white),
                  ),
                  isScrollable: true,
                  tabs: [
                    Tab(text: 'Dashboard'),
                    Tab(text: 'Favorites'),
                    Tab(text: 'Profile'),
                  ],
                ),
              centerTitle: true,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text('Home Star Wars Fandom', style: titleFont.copyWith(color: white),),
                  IconButton(
                      onPressed: () {
                        SlidePopUpCustomContent(
                          context: context,
                          title: 'Logout',
                          child: Center(
                              child: Column(
                                children: <Widget>[
                                  Text('Are you sure you want to Logout?', style: titleFont,),
                                  VButton(
                                      title: "Logout",
                                      onTap: () {
                                        Navigator.pop(context);
                                        state!.logout();
                                        Navigator.push(context, MaterialPageRoute(
                                          builder: (context) {
                                            return MyApp();
                                          },
                                        ));
                                      }
                                  ),
                                ],
                              )
                          ),
                        ).build();
                      },
                      icon: Icon(Icons.logout, color: gray)),
                ],
              ),
            ),
            body: TabBarView(
              children: [
                DashboardView(),
                FavoriteView(),
                ProfileView()
              ],
            )

        )
    );
  }
}