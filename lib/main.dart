import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:star_wars/models/user_model.dart';
import 'package:star_wars/providers/user_auth_state.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/views/home_view.dart';
import 'package:star_wars/views/login_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  Future<UserModel> getUserData () => UserPreferences().getInitApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ListenableProvider(
        create: (context) => UserAuthState(),
        child: MaterialApp(
          title: 'Star Wars',
          theme: ThemeData(
            primarySwatch: mainBackgroud,
          ),
          home: FutureBuilder(
              future: getUserData(),
              builder: (context, AsyncSnapshot<UserModel> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                    return CircularProgressIndicator();
                  default:
                    if (snapshot.hasError)
                      return Text('Error: ${snapshot.error}');
                    else if (snapshot.data!.id == null)
                      return LoginView();
                    else
                      Provider.of<UserAuthState>(context).setUser(snapshot.data!);
                    return HomeView();

                }
              }),
        )
    );
  }
}
