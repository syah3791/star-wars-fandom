class UserModel{
  int? id;
  String? name;
  String? email;
  String? password;

  UserModel({this.id, this.name, this.password, this.email});

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    password: json["password"],
  );
  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "email": email,
    "password": password,
  };
}