class PeopleModel{
  int? id;
  String? name;
  String? height;
  String? mass;
  String? hair_color;
  String? skin_color;
  String? eye_color;
  String? birth_year;
  String? gender;
  String? homeworld;
  List<String>? films;
  List<String>? species;
  List<String>? vehicles;
  List<String>? starships;
  String? created;
  String? edited;
  String? url;
  bool? favorite;

  PeopleModel({this.id, this.name, this.height, this.mass, this.hair_color, this.skin_color,
    this.eye_color, this.birth_year, this.gender, this.homeworld, this.films, this.species,
    this.vehicles, this.starships, this.created, this.edited, this.url, this.favorite});

  factory PeopleModel.fromJson(Map<String, dynamic> json) {
    List<String>? films;
    List<String>? species;
    List<String>? vehicles;
    List<String>? starships;
    try {
      films = json["films"].split(",");
      species = json["species"].split(",");
      vehicles = json["vehicles"].split(",");
      starships = json["starships"].split(",");
    }catch (e){
      films = json["films"];
      species = json["species"];
      vehicles = json["vehicles"];
      starships = json["starships"];
    }
    return PeopleModel(
      id: json["id"],
      name: json["name"],
      height: json["height"],
      mass: json["mass"],
      hair_color: json["hair_color"],
      skin_color: json["skin_color"],
      eye_color: json["eye_color"],
      birth_year: json["birth_year"],
      gender: json["gender"],
      homeworld: json["homeworld"],
      films: films,
      species: species,
      vehicles: vehicles,
      starships: starships,
      created: json["created"],
      edited: json["edited"],
      url: json["url"],
      favorite: json["favorite"] == 1,
    );
  }
  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "height": height,
    "mass": mass,
    "hair_color": hair_color,
    "skin_color": skin_color,
    "eye_color": eye_color,
    "birth_year": birth_year,
    "gender": gender,
    "homeworld": homeworld,
    "films": films,
    "species": species,
    "vehicles": vehicles,
    "starships": starships,
    "url": url,
    "favorite": favorite,
  };
}