import 'dart:async';
import 'package:star_wars/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  Future<UserModel> getInitApp() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    //User
    int? id = prefs.getInt("id");
    String? name = prefs.getString("name");
    String? email = prefs.getString("email");
    return UserModel(
      id: id,
      name: name,
      email: email,
    );
  }

  Future<bool> saveUser(UserModel user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setInt("id", user.id!);
    prefs.setString("name", user.name!);
    prefs.setString("email", user.email!);

    return prefs.commit();
  }

  void signOut() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.remove("id");
    prefs.remove("name");
    prefs.remove("email");
  }

}
