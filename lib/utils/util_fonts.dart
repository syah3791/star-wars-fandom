import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import './util_colors.dart';
//Fonts
var heading1 = GoogleFonts.roboto(
  textStyle: TextStyle(
    fontSize: 20.0,
    color: black,
    fontWeight: FontWeight.w500,
  ),
);
var titleFont = GoogleFonts.roboto(
  textStyle: TextStyle(
    fontSize: 16.0,
    color: black,
    fontWeight: FontWeight.w500,
  ),
);
var bodyFont = GoogleFonts.roboto(
  textStyle: TextStyle(
    fontSize: 14.0,
    color: black,
    fontWeight: FontWeight.w400,
  ),
);
var bodyMediumFont = GoogleFonts.roboto(
  textStyle: TextStyle(
    fontSize: 14.0,
    color: black,
    fontWeight: FontWeight.w500,
  ),
);
var descriptionFont = GoogleFonts.roboto(
  textStyle: TextStyle(
    fontSize: 12.0,
    color: black,
    fontWeight: FontWeight.w400,
  ),
);
