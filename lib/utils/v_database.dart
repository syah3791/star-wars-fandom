import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:star_wars/models/peopel_model.dart';
import 'package:star_wars/models/user_model.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database? _database;

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "StarWarsDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          Batch batch = db.batch();
          batch.execute("CREATE TABLE Users ("
              "id INTEGER PRIMARY KEY,"
              "name TEXT,"
              "email TEXT,"
              "password TEXT"
              ")");
          batch.execute("CREATE TABLE Peoples ("
              "id INTEGER PRIMARY KEY,"
              "name TEXT,"
              "height TEXT,"
              "mass TEXT,"
              "hair_color TEXT,"
              "skin_color TEXT,"
              "eye_color TEXT,"
              "birth_year TEXT,"
              "gender TEXT,"
              "homeworld TEXT,"
              "films TEXT,"
              "species TEXT,"
              "vehicles TEXT,"
              "starships TEXT,"
              "url TEXT,"
              "favorite INTEGER"
              ")");
          List<dynamic> res = await batch.commit();
        });
  }

  newUser(UserModel newUser) async {
    print(newUser.name);
    final db = await database;
    //get the biggest id in the table
    var table = await db!.rawQuery("SELECT MAX(id)+1 as id FROM Users");
    int id = (table.first["id"] ?? 1) as int;
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Users (id,name,email,password)"
            " VALUES (?,?,?,?)",
        [id, newUser.name, newUser.email, newUser.password]);
    return id;
  }

  updateUser(UserModel newUser) async {
    final db = await database;
    var res = await db!.update("Users", newUser.toMap(),
        where: "id = ?", whereArgs: [newUser.id]);
    return res;
  }

  getUser(int id) async {
    final db = await database;
    var res = await db!.query("Users", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? UserModel.fromJson(res.first) : null;
  }

  Future<bool>  checkEmail(String email) async {
    final db = await database;
    var res = await db!.query("Users", where: "email = ?", whereArgs: [email]);
    print(res);
    return res.isNotEmpty ? true : false;
  }

  login(String email, String password) async {
    final db = await database;
    var res = await db!.query("Users", where: "email = ? and password = ?", whereArgs: [email, password]);
    return res.isNotEmpty ? UserModel.fromJson(res.first) : null;
  }

  Future<List<UserModel>> getAllUsers() async {
    final db = await database;
    var res = await db!.query("Users");
    List<UserModel> list =
    res.isNotEmpty ? res.map((c) => UserModel.fromJson(c)).toList() : [];
    return list;
  }

  deleteUser(int id) async {
    final db = await database;
    return db!.delete("Users", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db!.rawDelete("Delete  from Users");
  }

  newPeople(PeopleModel newPeople) async {
    print(newPeople.name);
    final db = await database;
    //get the biggest id in the table
    var table = await db!.rawQuery("SELECT MAX(id)+1 as id FROM Peoples");
    int id = (table.first["id"] ?? 1) as int;
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Peoples (id,name,height,mass,hair_color,skin_color,eye_color,birth_year,gender,homeworld,films,species,vehicles,starships,url,favorite)"
            " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
        [id, newPeople.name, newPeople.height, newPeople.mass, newPeople.hair_color, newPeople.skin_color
          , newPeople.eye_color, newPeople.birth_year, newPeople.gender, newPeople.homeworld, newPeople.films, newPeople.species
          , newPeople.vehicles, newPeople.starships, newPeople.url, false]);
    return id;
  }

  updatePeople(PeopleModel newPeople) async {
    final db = await database;
    var res = await db!.update("Peoples", newPeople.toMap(),
        where: "id = ?", whereArgs: [newPeople.id]);
    return res;
  }

  Future<List<PeopleModel>> getAllPeoples(bool isASC) async {
    final db = await database;
    var res = isASC ? await db!.query("Peoples", orderBy:'name ASC') : await db!.query("Peoples", orderBy:'name DESC');
    List<PeopleModel> list =
    res.isNotEmpty ? res.map((c) => PeopleModel.fromJson(c)).toList() : [];
    return list;
  }

  Future<List<PeopleModel>> getAllPeoplesFavorite(bool isASC) async {
    final db = await database;
    var res = isASC ? await db!.query("Peoples", orderBy:'name ASC', where: "favorite = ?", whereArgs: [1]) : await db!.query("Peoples", orderBy:'name DESC', where: "favorite = ?", whereArgs: [1]);
    List<PeopleModel> list =
    res.isNotEmpty ? res.map((c) => PeopleModel.fromJson(c)).toList() : [];
    return list;
  }

  deletePeople(int id) async {
    final db = await database;
    return db!.delete("Peoples", where: "id = ?", whereArgs: [id]);
  }

  deleteAllPeople() async {
    final db = await database;
    db!.rawDelete("Delete from Peoples");
  }

  checkDatabase() async {
    final db = await database;
    var res = await db!.rawQuery("SELECT * FROM Peoples");
    return res.isNotEmpty ? true : false;
  }
}