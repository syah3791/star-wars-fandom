import 'package:flutter/material.dart';
//Padding
var paddingTop8 = EdgeInsets.only(top: 8.0);
var paddingBottom12 = EdgeInsets.only(bottom: 12.0);
var padding8 = EdgeInsets.all(8.0);
var paddingList = EdgeInsets.fromLTRB(18.0, 16.0, 18.0, 16.0);
var paddingButton = EdgeInsets.fromLTRB(18.0, 10.0, 18.0, 10.0);
var paddingInput = EdgeInsets.fromLTRB(18.0, 22.0, 18.0, 22.0);
