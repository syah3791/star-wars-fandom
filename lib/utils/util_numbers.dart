import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String doubelToString ({required double value, String format: 'en_US', String percision: '0.##'}) {
  var f = NumberFormat(percision, format);
  return f.format(value);
}