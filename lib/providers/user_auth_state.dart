import 'package:flutter/material.dart';
import 'package:star_wars/models/user_model.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/utils/v_database.dart';

class UserAuthState with ChangeNotifier{
  UserModel? _user;
  UserModel? get user => _user;

  setUser(UserModel user) => _user = user;

  checkEmail(String email) {
    return _validateEmail(email);
  }

  bool _validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  Future<bool> register(UserModel newUser) async {
    bool status = await DBProvider.db.checkEmail(newUser.email!);
    print(status);
    if (!status){
      await DBProvider.db.newUser(newUser);
      return true;
    } return false;
  }

  Future<bool> login(String email, String password) async {
    bool status = await DBProvider.db.checkEmail(email);
    if (status){
      UserModel _user = await DBProvider.db.login(email, password) ?? UserModel();
      if (_user.id != null){
        UserPreferences().saveUser(_user);
        return true;
      } return false;
    } return false;
  }

  logout() async {
    await DBProvider.db.deleteAllPeople();
    UserPreferences().signOut();
  }
}