import 'package:flutter/material.dart';
import 'package:star_wars/models/peopel_model.dart';
import 'package:star_wars/services/services.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/utils/v_database.dart';

class FavoriteMainState with ChangeNotifier {
  bool _isLoading = false;
  List<PeopleModel> _peopleList = [];
  List<PeopleModel> _displayList = [];
  List<String> _genders = ['male', 'female'];
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  final GlobalKey _visibilityDetector = GlobalKey();
  final _formKey = GlobalKey<FormState>();
  bool _isGrid = false;
  bool _isASC = true;
  PeopleModel details = PeopleModel(birth_year: DateUtil.dateToString(date: DateTime.now()));

  bool get isLoading =>  _isLoading;
  List<PeopleModel> get displayList => _displayList;
  List<String> get genders => _genders;
  get scaffoldState => _scaffoldState;
  get visibilityDetector => _visibilityDetector;
  get formKey => _formKey;
  bool get isGrid =>  _isGrid;
  bool get isASC =>  _isASC;

  init() async {
    bool _check = await DBProvider.db.checkDatabase();
    if (_check){
      _getListPeoples();
    } else {
      _isLoading = true;
      notifyListeners();
      ApiClient().get(
          url: APIUrl.people,
          callback: (status, message, res) async {
            _isLoading = false;
            print(res);
            if (message == true) {
              if(res["breed_all"].length > 0) {
                _peopleList = List<PeopleModel>.from(res["breed_all"].map((result) => PeopleModel.fromJson(result)));
              }
            } else {
              _scaffoldState.currentState!.showSnackBar(SnackBar(
                content: Text('$res'),
              ));
            }
            notifyListeners();
            return;
          });
    }
  }

  searchQuaries(String text){
    _displayList = _peopleList.where((item) {
      var noteTitle = item.name!.toLowerCase();
      return noteTitle.contains(text);
    }).toList();
    notifyListeners();
  }

  changeModeView(){
    _isGrid = !_isGrid;
    notifyListeners();
  }

  changeOrder() {
    _isASC = !_isASC;
    _getListPeoples();
  }

  changeFavorite(int index) {
    details = _displayList[index];
    details.favorite = !(_displayList[index].favorite!);
    saveDetails(false);
  }

  addPeoplesAndDetails() {
    details = PeopleModel(birth_year: DateUtil.dateToString(date: DateTime.now()));
  }

  editPeoplesAndDetails(int index) {
    details = _displayList[index];
  }

  saveDetails(bool isAdd) async {
    _isLoading = true;
    notifyListeners();
    if (isAdd) {
      await DBProvider.db.newPeople(details);
    } else {
      await DBProvider.db.updatePeople(details);
    }
    _isLoading = false;
    notifyListeners();
  }

  _getListPeoples() async {
    _isLoading = true;
    notifyListeners();
    _peopleList = await DBProvider.db.getAllPeoplesFavorite(_isASC) as List<PeopleModel>;
    _displayList = List.from(_peopleList);
    _isLoading = false;
    notifyListeners();
  }
}