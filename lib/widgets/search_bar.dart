import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:star_wars/utils/utils.dart';

class SearchBar extends StatelessWidget {
  final  onChanged ;
  SearchBar({this.onChanged});

  @override
  Widget build(BuildContext context) {
    var _searchController = TextEditingController();
    return Container(
      padding: paddingButton,
      decoration: myBoxDecoration(),
      child: TextField(
        decoration: InputDecoration(
            isDense: true,
            hintText: "Search...",
            prefixIcon: Icon(Icons.search, color: green50, size:24.0),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: white),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              borderSide: BorderSide(color: white),
            ),
            fillColor: divider,
            filled: true
        ),
        textCapitalization: TextCapitalization.sentences,
        // onChanged: (text) {
        //   text = text.toLowerCase();
        //   searchQuaries(text);
        //   },
        onChanged: onChanged,
        onSubmitted: (text) {

        },
      ),
    );
  }
  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      color:white,
      border: Border(
          bottom: BorderSide(
              color: divider,
              width: 1.0
          )
      ),
    );
  }
}
