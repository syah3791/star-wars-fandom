import 'package:flutter/material.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/widgets/flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';

class VDatePicker {
  final onConfirm;
  DateTime? date;
  BuildContext? context;

  VDatePicker({required this.context, this.date, this.onConfirm});

  build() {
    var mediaQueryData = MediaQuery.of(context!);
    final double widthScreen = mediaQueryData.size.width;
    final double heightScreen = mediaQueryData.size.height;
    DatePicker.showDatePicker(
      context!,
      initialDateTime: date,
      pickerMode: DateTimePickerMode.date,
      dateFormat: DateUtil.DATE_SLASH_FORMAT,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        pickerHeight: heightScreen/2.5,
        cancel:Text('Select Date',
          style: titleFont,),
        confirm: Text('confirm',
          style: bodyMediumFont.copyWith(color:white),
        ),
      ),
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onConfirm: onConfirm,
    );
  }
}
