// import 'package:flutter/material.dart';
// import 'package:layerfarm_manager/utils/utils.dart';
// import 'package:layerfarm_manager/localization/language_constants.dart';
// import 'package:layerfarm_manager/widgets/decoration.dart';
// import 'package:layerfarm_manager/widgets/search_bar.dart';
//
// class SlidePopUpAddEditAndSearch{
//   //Jangan lupa membuat translate select_$title dan create_$title
//   final BuildContext context;
//   final String? title;
//   var item;
//   final Function()? onAdd;
//   final Function(int id)? onChange;
//   final Function(int id)? onEdit;
//   final Function(String query)? onSearch;
//
//   SlidePopUpAddEditAndSearch({required this.context, this.title, this.item, this.onAdd, this.onChange, this.onEdit, this.onSearch});
//   void build(){
//     var mediaQueryData = MediaQuery.of(context);
//     final double? widthScreen = mediaQueryData.size.width;
//     final double? heightScreen = mediaQueryData.size.height;
//     slideDialog.showSlideDialog(
//         context: context,
//         barrierColor: Colors.white.withOpacity(0.7),
//         pillColor: Colors.red,
//         backgroundColor: Colors.white,
//         child: Container(
//             height: heightScreen! - 260,
//             width: widthScreen,
//             child: Stack(
//                 children: <Widget>[
//                   Positioned(
//                       top: 0,
//                       right: 20,
//                       left: 20,
//                       child: Container(
//                         padding: paddingButton,
//                         decoration: myBoxDecoration(),
//                         child: TextField(
//                           decoration: InputDecoration(
//                               isDense: true,
//                               hintText: "Search...",
//                               prefixIcon: Icon(Icons.search, color: green50, size:24.0),
//                               focusedBorder: OutlineInputBorder(
//                                 borderRadius: BorderRadius.all(Radius.circular(8.0)),
//                                 borderSide: BorderSide(color: white),
//                               ),
//                               enabledBorder: OutlineInputBorder(
//                                 borderRadius: BorderRadius.all(Radius.circular(8.0)),
//                                 borderSide: BorderSide(color: white),
//                               ),
//                               fillColor: divider,
//                               filled: true
//                           ),
//                           textCapitalization: TextCapitalization.sentences,
//                             onChanged: (query) => onSearch!(query),
//                           // onChanged: (text) {
//                           //   text = text.toLowerCase();
//                           //   searchQuaries(text);
//                           // },
//                           onSubmitted: (text) {
//
//                           },
//                         ),
//                       ),
//                   ),
//                   Positioned(
//                     top: 40,
//                     right: 2,
//                     left: 2,
//                     child: SingleChildScrollView(
//                       child: ListView.builder(
//                         itemCount: item.length,
//                         scrollDirection: Axis.vertical,
//                         shrinkWrap: true,
//                         itemBuilder: (context, index) {
//                           return _list(context, index);
//                         },
//                       ),
//                     ),
//                   ),
//                   Positioned(
//                       bottom: 0,
//                       right: 0,
//                       left: 0,
//                       child: Container(
//                           padding: paddingButton,
//                           decoration: myBoxDecoration(),
//                           child: MaterialButton(
//                               onPressed: ()=> onAdd!(),
//                               // onPressed: () async {
//                               //   onTapAdd();
//                               // },
//                               padding: paddingButton.copyWith(
//                                   top: 12.0, bottom: 12.0),
//                               shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.circular(6.0)),
//                               color: Theme
//                                   .of(context)
//                                   .primaryColor,
//                               child: Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: <Widget>[
//                                     Icon(Icons.add_rounded, color: white),
//                                     SizedBox(
//                                       width: 4,
//                                     ),
//                                     Text(
//                                       getTranslated(context, 'create_$title'),
//                                       style: bodyMediumFont.copyWith(color: white),
//                                     ),
//                                   ]
//                               )
//                           )
//                       )
//                   ),
//                 ]
//             )
//         )
//     );
//   }
//   Widget _list(context, index) {
//     return Container(
//         padding: paddingInput.copyWith(top: 0, bottom: 0),
//         child: Column(
//             children: <Widget>[
//               ListTile(
//                 title: Text(item[index].name, style: bodyFont),
//                 onTap: () => onChange!(index),
//                 trailing: IconButton(
//                   icon: Icon(Icons.edit),
//                   onPressed: () => onEdit!(item[index].id),
//                 ),
//               ),
//               Divider(thickness: 1.0),
//             ]
//         )
//     );
//   }
//   // searchQuaries(String query) => onSearch!(query);
// }
