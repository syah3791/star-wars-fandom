import 'package:flutter/material.dart';
import 'package:star_wars/main.dart';
import 'package:star_wars/utils/utils.dart';

class VDraggableScrollableSheet{
  //Jangan lupa membuat translate select_$title dan create_$title
  final BuildContext context;
  String? title;
  String? subtitle;
  Widget child;
  Color pillColor;

  VDraggableScrollableSheet({required this.context, this.title: '', this.subtitle: '',
    required this.child, this.pillColor: Colors.blueGrey});
  build() {
    var mediaQueryData = MediaQuery.of(context);
    final double widthScreen = mediaQueryData.size.width;
    final double heightScreen = mediaQueryData.size.height;
    try {
      title = "${title}";
    } catch (e){

    }
    showModalBottomSheet<void>(
      backgroundColor: Colors.transparent,
      context: context,
      isScrollControlled:true,
      builder: (BuildContext context) {
        return DraggableScrollableSheet(
          initialChildSize: 0.5,
          maxChildSize: 1,
          builder: (BuildContext context, ScrollController scrollController) {
            return SingleChildScrollView(
                controller: scrollController,
                child: ClipRRect(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(20.0)),
                    child: Container(
                      color: green20,
                      height: heightScreen,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(height: 10.0),
                                Container(
                                  height: 5.0,
                                  width: 25.0,
                                  decoration: BoxDecoration(
                                    color: pillColor,
                                    borderRadius: BorderRadius.circular(500.0),
                                  ),
                                ),
                                SizedBox(height: 20.0),
                              ],
                            ),
                          ),
                          Container(
                            child: ListTile(
                              title: Text('$title', style: titleFont.copyWith(color: mainGreen),),
                              trailing: IconButton(
                                  onPressed: () => Navigator.pop(context),
                                  icon: Icon(Icons.cancel)
                              ),
                              selectedTileColor: mainBackgroud,
                            ),
                          ),
                          Divider(thickness: 1.0),
                          Expanded(
                            child: child,
                          )
                        ],
                      ),
                    )
                )
            );
          },
        );
      },
    );
  }
}
