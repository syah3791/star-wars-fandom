import 'package:flutter/material.dart';
import 'package:star_wars/utils/utils.dart';

class SlidePopUpCustomContent{
  //Jangan lupa membuat translate select_$title dan create_$title
  final BuildContext context;
  final String? title;
  Widget child;
  Color pillColor;

  SlidePopUpCustomContent({required this.context, required this.title, required this.child, this.pillColor: Colors.blueGrey});
  void build(){
    var mediaQueryData = MediaQuery.of(context);
    final double widthScreen = mediaQueryData.size.width;
    final double heightScreen = mediaQueryData.size.height;
    showModalBottomSheet<void>(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return ClipRRect(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0)),
          child: Container(
            color: white,
            child: Wrap(
              spacing: 10,
              children: <Widget>[
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      Container(
                        height: 5.0,
                        width: 25.0,
                        decoration: BoxDecoration(
                          color: pillColor,
                          borderRadius: BorderRadius.circular(500.0),
                        ),
                      ),
                      SizedBox(height: 20.0),
                    ],
                  ),
                ),
                Container(
                  padding: paddingInput.copyWith(top: 0),
                  color: Colors.white,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${title}",
                              style: titleFont,),
                            IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.clear)
                            ),
                          ],
                        ),
                        child
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
