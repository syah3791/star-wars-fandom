import 'package:flutter/material.dart';
import 'package:star_wars/utils/utils.dart';
import 'package:star_wars/widgets/decoration.dart';
import 'package:star_wars/widgets/slide_pop_up/slide_pop_up_custom_content.dart';

class SlidePopUpAddAndEdit{
  //Jangan lupa membuat translate select_$title dan create_$title
  final BuildContext context;
  final String? title;
  var item;
  final Function()? onAdd;
  final Function(int id)? onChange;
  final Function(int id)? onEdit;

  SlidePopUpAddAndEdit({required this.context, this.title, this.item, this.onAdd, this.onChange, this.onEdit});
  void build(){
    var mediaQueryData = MediaQuery.of(context);
    final double widthScreen = mediaQueryData.size.width;
    final double heightScreen = mediaQueryData.size.height;
    SlidePopUpCustomContent(
        context: context,
        title: title,
        child: Container(
            height: heightScreen - 400,
            width: widthScreen,
            child: Stack(
                children: <Widget>[
                  Positioned(
                      top: 0,
                      right: 2,
                      left: 2,
                      child: Container(
                        height: 230,
                        child: Scrollbar(
                          child: ListView.builder(
                            itemCount: item.length,
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return _list(context, index);
                            },
                          ),
                        ),
                      )
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      child: Container(
                          padding: paddingButton.copyWith(bottom:0),
                          decoration: myBoxDecoration(),
                          child: MaterialButton(
                              onPressed: ()=> onAdd!(),
                              padding: paddingButton.copyWith(
                                  top: 12.0, bottom: 12.0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0)),
                              color: Theme
                                  .of(context)
                                  .primaryColor,
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.add_rounded, color: white),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      "${title}",
                                      style: bodyMediumFont.copyWith(color: white),
                                    ),
                                  ]
                              )
                          )
                      )
                  ),
                ]
            )
        )
    ).build();
  }
  Widget _list(context, index) {
    return Container(
        padding: paddingInput.copyWith(top: 0, bottom: 0),
        child: Column(
            children: <Widget>[
              ListTile(
                title: Text(item[index].name, style: bodyFont),
                onTap: () => onChange!(index),
                trailing: IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () => onEdit!(item[index].id),
                ),
              ),
              Divider(thickness: 1.0),
            ]
        )
    );
  }
}
