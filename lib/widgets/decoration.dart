import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:star_wars/utils/utils.dart';

InputDecoration inputDec(ctx, label){
  return InputDecoration(
      isDense: true,
      border: OutlineInputBorder(),
      labelText: "${label}",
      labelStyle: bodyFont,
      hintText: "${label}",
      hintStyle: bodyFont.copyWith(color: gray)
  );
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    color:white,
    border: Border(
        top: BorderSide(
            color: divider,
            width: 1.0
        )
    ),
  );
}