import 'package:flutter/material.dart';
import 'package:star_wars/widgets/decoration.dart';
import 'package:star_wars/utils/utils.dart';

class VButton extends StatelessWidget {
  VButton({Key? key, this.title, required this.onTap}) : super(key: key);
  String? title;
  final onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: paddingButton,
        decoration: myBoxDecoration(),
        child: RaisedButton(
          onPressed: onTap,
          padding:paddingButton.copyWith(top:12.0, bottom:12.0),
          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(6.0) ),
          color: Theme.of(context).primaryColor,
          child: Center(
            child: Text(
              '${title}',
              style: titleFont.copyWith(color: white),
            ),
          ),
        )
    );
  }
}
