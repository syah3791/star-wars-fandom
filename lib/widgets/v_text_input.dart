import 'package:flutter/material.dart';
import 'package:star_wars/widgets/decoration.dart';
import 'package:star_wars/utils/utils.dart';

class VInputText extends StatelessWidget {
  final TextInputType? keyboardType;
  final String hint;
  final TextEditingController? controller;
  final TextCapitalization capitalization;
  final validator;
  final onSaved;
  final onChanged;
  final onTap;
  final initialValue;
  final suffixIcon;
  final suffixText;
  final helperText;
  final helperStyle;
  final suffixStyle;
  final prefixIcon;
  final prefixText;
  final prefixStyle;
  final bool enabled;
  final bool readOnly;
  final bool isValid;
  final bool isNumber;
  final bool obscureText;

  VInputText(
      this.hint, {
        this.keyboardType,
        this.readOnly = false,
        this.controller,
        this.validator,
        this.onSaved,
        this.onTap,
        this.initialValue,
        this.enabled = true,
        this.isValid = true,
        this.isNumber = false,
        this.obscureText = false,
        this.onChanged,
        this.capitalization = TextCapitalization.sentences,
        this.suffixIcon,
        this.suffixText,
        this.suffixStyle,
        this.prefixIcon,
        this.prefixText,
        this.prefixStyle,
        this.helperText,
        this.helperStyle,
      });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enabled,
      initialValue: initialValue ?? null,
      controller: controller ?? null,
      decoration: InputDecoration(
        fillColor: Colors.white,
        isDense : true,
        filled: true,
        labelText: "${hint}",
        labelStyle: bodyFont,
        hintText: "${hint}",
        hintStyle: bodyFont.copyWith(color: gray),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
          borderRadius: BorderRadius.circular(8.0),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey[300]!),
          borderRadius: BorderRadius.circular(8.0),
        ),
        helperText: helperText,
        helperStyle: helperStyle,
        suffixIcon: suffixIcon,
        suffixText: suffixText,
        suffixStyle: suffixStyle,
        prefixIcon: prefixIcon,
        prefixText: prefixText,
        prefixStyle: prefixStyle,
      ),
      keyboardType: isNumber ? TextInputType.number : this.keyboardType ?? TextInputType.text,
      textCapitalization: capitalization,
      validator: isValid ? validator ??  (val) =>  (val!.length == 0 ? 'Please enter value' : null) : null,
      onSaved: onSaved,
      onChanged: onChanged,
      onTap: onTap,
      readOnly: readOnly,
      obscureText: obscureText,
    );
  }
}
