import 'package:flutter/material.dart';

import '../date_picker_constants.dart';
import '../date_picker_theme.dart';
import '../i18n/date_picker_i18n.dart';
import 'package:star_wars/widgets/decoration.dart';
import 'package:star_wars/utils/utils.dart';

/// DatePicker's title widget.
///
/// @author dylan wu
/// @since 2019-05-16
class DatePickerTitleWidget extends StatelessWidget {
  DatePickerTitleWidget({
    Key? key,
    this.pickerTheme,
    this.locale,
    required this.onCancel,
    required this.onConfirm,
  }) : super(key: key);

  final DateTimePickerTheme? pickerTheme;
  final DateTimePickerLocale? locale;
  final DateVoidCallback onCancel, onConfirm;

  @override
  Widget build(BuildContext context) {
    if (pickerTheme!.title != null) {
      return pickerTheme!.title!;
    }
    return Stack(alignment: AlignmentDirectional.topStart,children: <Widget>[
            _renderCancelWidget(context),
            _renderConfirmWidget(context),
      ]
      ,
    );
  }

  /// render cancel button widget
  Widget _renderCancelWidget(BuildContext context) {
    if (isCustomTitleWidget()) {
      // has custom title button widget
      if (pickerTheme!.cancel == null) {
        return Offstage();
      }
    }

    Widget cancelWidget = pickerTheme!.cancel!;
    if (cancelWidget == null) {
      TextStyle textStyle = pickerTheme!.cancelTextStyle ??
          TextStyle(
            color: Theme.of(context).unselectedWidgetColor,
            fontSize: 16.0,
          );
      cancelWidget = Text(
        DatePickerI18n.getLocaleCancel(locale!),
        style: textStyle,
      );
    }

    return Positioned(
        top: 18,
        right: 20,
        left: 20,
        child:Row(
          children: <Widget>[
            cancelWidget,
            Spacer(),
            IconButton(
              onPressed:  () => this.onCancel(),
              icon: Icon(Icons.clear)
          ),
          ]
        )
    );
  }

  /// render confirm button widget
  Widget _renderConfirmWidget(BuildContext context) {
    if (isCustomTitleWidget()) {
      // has custom title button widget
      if (pickerTheme!.confirm == null) {
        return Offstage();
      }
    }

    Widget confirmWidget = pickerTheme!.confirm!;
    if (confirmWidget == null) {
      TextStyle textStyle = pickerTheme!.confirmTextStyle ??
          TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 16.0,
          );
      confirmWidget = Text(
        DatePickerI18n.getLocaleDone(locale!),
        style: textStyle,
      );
    }

    return Positioned(
        bottom: 0,
        right: 0,
        left: 0,
        child: Container(
            padding: paddingButton,
            decoration: myBoxDecoration(),
            child: MaterialButton(
              onPressed: () => this.onConfirm(),
              padding:paddingButton.copyWith(top:12.0, bottom:12.0),
              shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(6.0) ),
              color: Theme.of(context).primaryColor,
              child: confirmWidget,
            )
        )
    );
  }

  bool isCustomTitleWidget() {
    return pickerTheme!.cancel != null ||
        pickerTheme!.confirm != null ||
        pickerTheme!.title != null;
  }
}
