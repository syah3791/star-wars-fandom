// import 'package:flutter/material.dart';
//
// import 'package:layerfarm_manager/widgets/flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
// import 'package:layerfarm_manager/utils/utils.dart';
// import 'package:layerfarm_manager/localization/language_constants.dart';
//
// class DatePickerBottomSheet extends StatefulWidget {
//   DatePickerBottomSheet({Key? key}) : super(key: key);
//
//   @override
//   State<StatefulWidget> createState() => _DatePickerBottomSheetState();
// }
//
// const String MIN_DATETIME = '2010-05-12 10:47:00';
// const String MAX_DATETIME = '2021-11-25 22:45:10';
// const String INIT_DATETIME = '2019-05-17 18:13:15';
//
// class _DatePickerBottomSheetState extends State<DatePickerBottomSheet> {
//   bool _showTitle = true;
//
//   DateTimePickerLocale _locale = DateTimePickerLocale.en_us;
//   List<DateTimePickerLocale> _locales = DateTimePickerLocale.values;
//
//   // String _format = 'dd-MMMM-yyyy';
//   String _format = 'HH:mm';
//   TextEditingController _formatCtrl = TextEditingController();
//
//   DateTime? _dateTime;
//
//   @override
//   void initState() {
//     super.initState();
//     _formatCtrl.text = _format;
//     _dateTime = DateTime.parse(INIT_DATETIME);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     // create locale radio list
//     List<Widget> radios = [];
//     _locales.forEach((locale) {
//       radios.add(Container(
//         margin: EdgeInsets.only(right: 8.0),
//         child: Row(
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Radio(
//               value: locale,
//               groupValue: _locale,
//               onChanged: (value) {
//                 setState(() {
//                   _locale = value as DateTimePickerLocale;
//                 });
//               },
//             ),
//             Text(locale
//                 .toString()
//                 .substring(locale.toString().indexOf('.') + 1)),
//           ],
//         ),
//       ));
//     });
//
//     // TextStyle hintTextStyle =
//     // Theme.of(context).textTheme.subhead.apply(color: Color(0xFF999999));
//     // return Scaffold(
//     //   appBar: AppBar(title: Text('DatePicker Bottom Sheet')),
//     //   // display DatePicker floating action button
//     //   floatingActionButton: FloatingActionButton(
//     //     onPressed: _showTimePicker,
//     //     tooltip: 'Show DatePicker',
//     //     child: Icon(Icons.date_range),
//     //   ),
//     // );
//   }
//
//   /// Display date picker.
//   void _showDatePicker() {
//     var mediaQueryData = MediaQuery.of(context);
//     final double widthScreen = mediaQueryData.size.width;
//     final double heightScreen = mediaQueryData.size.height;
//     DatePicker.showDatePicker(
//       context,
//       initialDateTime: _dateTime!,
//       pickerMode: DateTimePickerMode.date,
//       dateFormat: _format,
//       locale: _locale,
//       onMonthChangeStartWithFirstDate: true,
//       pickerTheme: DateTimePickerTheme(
//         showTitle: _showTitle,
//         pickerHeight: heightScreen/2.5,
//         cancel:Text(getTranslated(context, 'select_date'),
//           style: titleFont,),
//         confirm: Text(
//           getTranslated(context, 'comfirm'),
//           style: bodyMediumFont.copyWith(color:white),
//         ),
//       ),
//       // minDateTime: DateTime.parse(MIN_DATETIME),
//       // maxDateTime: DateTime.parse(MAX_DATETIME),
//       onClose: () => print("----- onClose -----"),
//       onCancel: () => print('onCancel'),
//       onChange: (dateTime, List<int> index) {
//         setState(() {
//           _dateTime = dateTime;
//         });
//       },
//       onConfirm: (dateTime, List<int> index) {
//         setState(() {
//           print("comsa"+dateTime.toString());
//           _dateTime = dateTime;
//         });
//       },
//     );
//   }
//   /// Display date picker.
//   void _showTimePicker() {
//     var mediaQueryData = MediaQuery.of(context);
//     final double widthScreen = mediaQueryData.size.width;
//     final double heightScreen = mediaQueryData.size.height;
//     DatePicker.showDatePicker(
//       context,
//       initialDateTime: DateTime.parse(INIT_DATETIME),
//       dateFormat: 'HH:mm',
//       pickerMode: DateTimePickerMode.time, // show TimePicker
//       pickerTheme: DateTimePickerTheme(
//         showTitle: _showTitle,
//         pickerHeight: heightScreen/2.5,
//         cancel:Text(getTranslated(context, 'select_time'),
//           style: titleFont,),
//         confirm: Text(
//           getTranslated(context, 'comfirm'),
//           style: bodyMediumFont.copyWith(color:white),
//         ),
//       ),
//       onCancel: () {
//         debugPrint('onCancel');
//       },
//       onChange: (dateTime, List<int> index) {
//         setState(() {
//           _dateTime = dateTime;
//         });
//       },
//       onConfirm: (dateTime, List<int> index) {
//         setState(() {
//           print("sasa"+dateTime.toString());
//           _dateTime = dateTime;
//         });
//       },
//     );
//   }
// }